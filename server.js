var connect         = require('connect');
var connectStatic   = require('connect-gzip-static');
var port            = process.env.PORT || 9000;
var app             = connect();

app.use(connectStatic(__dirname  + '/'));

app.listen(port, function() {
  console.log('Server running on ' + port);
});
