## Prerequisites

node
npm

## Launch of the project

After downloading the project need to install dependencies by typing in the console command **npm i**
in the terminal in the directory with the project.

After that, you need to run **npm start**.
Then, go to the browser link:
**http://localhost:9000/**
