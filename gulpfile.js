'use strict';

var gulp = require('gulp');

var sass       = require('gulp-sass');
var browserify = require('browserify');
var watch      = require('gulp-watch');
var gutil      = require('gulp-util');
var source     = require('vinyl-source-stream');
var buffer     = require('vinyl-buffer');
var server     = require('gulp-develop-server');

var  sourceFile = './website/js/app.js';
var  destFile = 'bundle.js';
var  destFolder = './website/js/';

gulp.task('sass', function () {
  return gulp.src('./website/style/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./website/style/css'));
});

gulp.task('browserify', function() {
  return browserify(sourceFile)
  .bundle()
  .on('error', function(e) {
   gutil.log(e);
  })
  .pipe(source(destFile))
    .pipe(buffer())
    .pipe(gulp.dest(destFolder));
});

gulp.task('server:start', function() {
  server.listen({
    path: './server.js',
  });
});

gulp.task('watch', function () {
  gulp.watch("website/js/**/*.js", ['browserify']);
  gulp.watch("website/style/scss/**", ['sass']);
});

gulp.task('default', ['browserify', 'watch', 'server:start', 'sass']);
