"use strict";

module.exports = function(module) {

  module.config(config);

  function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
      /* HomePage */
      .state('home', {
        url: '/',
        templateUrl: 'website/templates/pages/home.html',
        controller: 'homePageCtrl',
        controllerAs: 'vm'
      });
  };
};
