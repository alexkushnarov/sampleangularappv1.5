'use strict';

module.exports = function (module) {
	module.filter('unixFromNow', function() {
		return function (unix) {
			return moment.unix(unix).fromNow();
		};
	});
};
