module.exports = (function() {
  "use strict";

  var config          = require('./config/initConfig.js');
	var initControllers = require('./controllers/initControllers.js');
  var initDirectives  = require('./directives/initDirectives.js');
  var initServices    = require('./services/initServices.js');
  var initFilters    = require('./filters/initFilters.js');

  var module = angular.module("sampleApp", [
    'ui.router'
  ]);

  config(module);
  initControllers(module);
  initDirectives(module);
  initServices(module);
  initFilters(module);
}());
