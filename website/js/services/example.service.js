'use strict';

module.exports = function (module) {
	module.factory('exampleService', ['$http', function($http) {
		return {
			getData: getData
		}

		/**
		* @ngdoc function
		* @name getData
		* @methodOf apiService
		*
		* @description
		* get data from reddit
		*
		* @return {Promise}
		*/
		function getData() {
			return $http.get('https://www.reddit.com/r/all.json');
		}
	}]);
};
