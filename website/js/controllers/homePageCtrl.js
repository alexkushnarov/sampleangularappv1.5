'use strict';

module.exports = function(module) {
  /**
  * @ngdoc controller
  * @name sampleApp:homePageCtrl
  * @description
  * The home page content controller.
  *
  */
  module
  .controller('homePageCtrl', homePageCtrl);
  // homePageCtrl.$inject = ['$scope']; // to inject dependencies
  // function homePageCtrl ($scope) {
  function homePageCtrl () {
    /**
    * @ngdoc property
    * @name vm
    *
    * @description
    * vm is an instance of the current controller.
    */
    var vm = this;
  }
};
